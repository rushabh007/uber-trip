import React from 'react'
import { withGoogleMap, GoogleMap } from 'react-google-maps';

const MapView = (props) => {
  const GoogleMapExample = withGoogleMap(props => (
    <GoogleMap
      defaultCenter={{ lat: 40.756795, lng: -73.954298 }}
      defaultZoom={13}
    >
    </GoogleMap>
  ));
  return (
    <div>
      <GoogleMapExample
        containerElement={<div className="vh-100" />}
        mapElement={<div style={{ height: `100%` }} />}
      />
    </div>
  )
}

export default MapView
