import React from 'react'
import './detail.css'
import * as env from '../../AppConfig/env'

const Detail = (props) => {
  return (
    <div className="col-auto leftSec vh-100">
      <h1 className="h1Title mb-3">Rahul is waiting for pickup</h1>
      <div className="pro">
        <ul className="StepProgress">
          <li className="StepProgress-item is-done">
            <div className="row">
              <div className="col"> 10th cross st </div>
              <div className="col text-right">
                <p className="mb-0 fnt13">1:53 pm</p>
              </div>
            </div>
          </li>
          <li className="StepProgress-item is-done">
            <div className="row">
              <div className="col"> 10th cross st </div>
              <div className="col text-right">
                <p className="mb-0 fnt13">1:53 pm</p>
              </div>
            </div>
          </li>
          <li className="StepProgress-item current">
            <div className="row">
              <div className="col"> 10th cross st </div>
              <div className="col text-right">
                <p className="mb-0 fnt13">1:53 pm</p>
              </div>
            </div>
          </li>
          <li className="StepProgress-item">
            <div className="row">
              <div className="col"> 10th cross st 10th cross st </div>
              <div className="col text-right">
                <p className="mb-0 fnt13">1:53 pm</p>
              </div>
            </div>
          </li>
          <li className="StepProgress-item">Done</li>
        </ul>
      </div>
      <div className="driverDetail">
        <div className="row justify-content-between align-items-center">
          <div className="col-auto">
            <img src={env.carimage} width={130} className="carImg" alt="star" />
            <img src={env.profile} className="driImg" width={50} alt="star" />
            <p className="mb-0 driName">
              Shivakumar K
                <span className="px-1 align-text-bottom font-weight-bold">.</span> 4.78{" "}
              <img className="starImg" src={env.star} alt="star" />
            </p>
          </div>
          <div className="col text-right">
            <h5 className="vehNum mb-1">KA06D4755</h5>
            <p className="mb-0 fnt13">White Toyota Etios</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Detail