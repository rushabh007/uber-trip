import imag1 from '../Assets/Images/car.png'
import imag2 from '../Assets/Images/close.svg'
import imag3 from '../Assets/Images/profile-pic.png'
import imag4 from '../Assets/Images/star.svg'

export const carimage = imag1
export const close = imag2
export const profile = imag3
export const star = imag4
