import React from 'react';

import 'bootstrap/dist/css/bootstrap.css'
import './GlobalCss/app.css'

import MapView from './Components/MapView/MapView'
import Detail from './Components/ContainView/Detail'


function App() {
  return (
    <div className="mainCOntainer row vh-100">
      <div className="contains py-4 d-sm-block">
        <Detail/>
      </div>
      <div className="maps col px-0">
        <MapView/>
      </div>
    </div>
  );
}

export default App;
